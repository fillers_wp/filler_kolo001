<?php
/**
 * template page for sitemap
 * fasttime theme by Anggra21
 */
/*
Template Name: sitemap
*/
?>
<?php get_header(); ?>


	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2 class="post-title"><?php the_title(); ?></h2>
			<div class="entryp">
				<h2 class="post-title">All Posts</h2>
          <ul>
<?php 
$recent_posts = get_posts('numberposts=1000&orderby=title&order=asc&posts_per_page=-1');//angka 6 = jumlah postingan yang mau ditampilkan
foreach( $recent_posts as $post ) :
setup_postdata($post);
?>
<li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
<?php endforeach; ?>
          </ul>

          <h2 class="post-title">All Categories</h2>
          <ul>
            <?php wp_list_cats('optioncount=1');?>
          </ul>
          <h2 class="post-title">All Tags</h2>
          <p>
             <?php wp_tag_cloud(); ?>
          </p>
				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		<?php endwhile; endif; ?>

	</div>

<?php include (TEMPLATEPATH . '/sidebarpage.php'); ?>

<?php get_footer(); ?>
