<?php
/* KATA-KATA MUTIARA */
/*
- JUDUL PARENT
- JUDUL ATTACHMENT
- CATEGORY
- TANGGAL
- AUTHOR
- TOTAL GAMBAR
- TAGS

*/
$atitle		= $post->post_title;
$atitle		= str_replace(array('-','_','+'),' ',$atitle);
$atitle		= ucwords($atitle); //Upper Case Words

/*
ucfirst ();		// upper case first sentence
strtoupper();	// str to upper case
strtolower();	// str to lower case
*/

$parid 		= $post->post_parent;
$partitle 	= get_the_title($parid);
$cats		= get_the_category($parid);
$catname	= $cats[0]->name;

$date		= get_the_time('F jS, Y H:i:s A');
$author		= get_the_author();


/* iki kontene */

$amazing 	= '{amazing|amusing|appealing|astonishing|astounding|awesome|breathtaking|captivating|charming|cool|enchanting|excellent|exciting|extraordinary|fascinating|glamorous|inspiring|interesting|marvellous|marvelous|mesmerizing|outstanding|remarkable|stunning|surprising|terrific|wonderful}';
$amaspun 	= js_spin($amazing);
$photo	 	= '{photo|picture|pics|photograph|digital imagery|digital photography|image|images}';
$gambarspun	= js_spin($photo);
$part		= '{part|segment|section|other parts}';
$partspun	= js_spin($part);
$article	= '{article|document|post|publishing|piece of writing|content|written piece|editorial|report|write-up}';
$articlespun = js_spin($article);
$categorized = '{categorized|classified|grouped|sorted|labeled|listed|categorised|classed as|arranged|assigned}';
$catspun	= js_spin($categorized);

$published	= '{published|posted}';
$pubspun	= js_spin($published);


echo 'The '.$amaspun.' '.$title.' '.$gambarspun.' below, is '.$partspun.' of '.$partitle.' '.$articlespun.' which is '.$catspun.' within '.$catname.', ';$tags 		= get_the_tags($parid);

 if($tags) {

	$i = 1;

	$limit = 3;

	shuffle($tags);

	echo '';

	foreach($tags as $tag) {

		if($i > $limit) {

		  break;

		} else {

		$tagname = $tag->name;


		if($i == $limit) { $comma = '';} else { $comma = ',';}

		echo $tagname.$comma.' ';

		}

		$i++;
		}
	}
echo 'and '.$pubspun.' at '.$date.' by '.$author.'.';
?>