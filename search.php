<?php get_header(); ?>

<div id="wrapper">
	<div id="content">

	<?php if(get_option('business_ads-4')!=""){?>
	<div class="ads-4">
	<?php if (get_option('business_ads-4') <> "") { 
		echo stripslashes(stripslashes(get_option('business_ads-4'))); 
} ?>
	</div>
	<?php }?>
	<?php if (have_posts()) : ?>
<?php $count = 1 ?>
		<?php while (have_posts()) : the_post(); ?>
				
			<div class="post" id="post-<?php the_ID(); ?>">
<div id="mnt">
			<?php randimgn(); ?>
</div>
				<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a></h2>
				
				<div class="entry">
					<?php an_excerpt('an_excerptlength_index', ''); ?>
				</div>
	
		<div class="clear"></div>
		
				<p class="postmetadata"><?php the_time('F j, Y') ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php the_category(', ') ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php comments_popup_link('No Comments', '1 Comment ', '% Comments'); ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php main_tags();?></p>
			</div>
		<?php if ($count==2) { include('ads.php'); } ?>
		<?php $count = $count + 1; ?>
	
		<?php endwhile; ?>

		<div class="navigationmain">

			<?php js_navi(); ?>
		</div>
		
	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>

	<?php endif; ?>

	</div>

<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>
