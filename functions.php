<?php
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* JS SEO
* Version: 1.6
* SEO function for pages : Search, category, tag, 404.
* by pulling out titles, excerpt, and tags from post within.
* Arguments : 
* - $theqs : Main var, the query. String. This would be the search query for search page, category title for category page, etc.
* - $pagetype : type of the page. String. Eg : search,category,tag, or 404.
* - $detag : type of tag. String. Meant for the 3 tags in header. title, description, keywords.
* Usage : js_seo($theqs,'search','title',false);

  - 1.6: May 01, 2012 : Year limit bug. Thx 2 Adi 4 bug report.
  - 1.5: Apr 30, 2012 : Pagination bug 4 tag pages. Thx 2 Rofiq + Yoga 4 bug report.
  - 1.4: Apr 24, 2012 : Limit tags + fix tag slug [line:134]. Thx 2 Adi 4 bug report.
    JS SEO Update. Version: 1.4. Release date: Apr 24, 2012.
    Update terbaru:
    - limitasi keyword ngikut limitasi title plus int random tertentu.
    - fix query tag. dari konstruksi tag slug sanitize title, jadi baca lgsg slug dr request uri.
  - 1.3: Mar 11, 2012
  - 1.2: Nov 5, 2011
**/

function js_seo($terms, $where, $limit,$keyword) {
  global $wpdb;
  $maxpage = $wp_query->max_num_pages;
  //Search
  if ($where == 'search') {
    $terms = str_replace('+',' ',$terms);
    $thekw = ucwords($terms);
    //$search_count = 0;
    $paged = get_query_var('paged');

    if($paged) {$pgnum = $paged;} else {$pgnum = '1';}
    $total_perpage = $pgnum*10;
    $offset = $total_perpage - 10;

    $search = new WP_Query('s='.$terms.'&posts_per_page='.$limit.'&paged='.$paged.'&offset='.$offset.'');
    if($paged) {$paginated = ' Page '.$pgnum; } else {$paginated = '';}
    echo $thekw.''.$paginated.': ';

    if($search->have_posts()) : while($search->have_posts()) : $search->the_post();
    echo the_title().'. ';
    //$search_count++;
    endwhile; else:
    echo $thekw.' news, '.$thekw.' updates, '.$thekw.' latest, '.$thekw.' rumors, '.$thekw.' gossip, '.$thekw.' latest news';
    endif; wp_reset_query();

    if($keyword) {
      $tagsearch = new WP_Query('s='.$terms.'&posts_per_page='.$limit.'&paged='.$paged.'&offset='.$offset.'');
      if($tagsearch->have_posts()) : while($tagsearch->have_posts()) : $tagsearch->the_post();
      $posttags = get_the_tags();
      if ($posttags) {
        shuffle($posttags);
        $i = 1;
        $tl = $limit + mt_rand(0,3);
        foreach($posttags as $tag) {
          if($i > $tl) {break;} else {
            $tagname = $tag->name;
            $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
            echo ucwords($tagname).','; 
          }
          $i++;
        }
      }
      endwhile; endif; wp_reset_query();
      
    } else {}

  // Attachment : Image
  } elseif ($where == 'image') {

  //Category Pages
  } elseif ($where == 'category') {
    $thekw = $terms;


    //CLEAN UP : STARTS ----------------------------
    $rementity = array(
    '[&hellip;]', '&hellip;', '...', ' - ', '// ', ' http://', ' www.', '.com ', '.net ', '.org ', '.us ',
    '&#34;','&#39;','&#039;','&#8220;','&#8221;','&#8216;','&#8217;','&raquo;',
    '!&quot','&quot;','&lt;','&gt;','®',
    //Dont' edit this line below
    '#');
    $thekw = str_replace($rementity,' ',$thekw); 
    $thekw = wptexturize($thekw);
    $thekw = strtolower($thekw);
    $thekw = ucwords($thekw);
    //CLEAN UP : ENDS ----------------------------  

    $catid = get_cat_ID(''.$terms.'');
    $paged = get_query_var('paged');
    if($paged) {$pgnum = $paged;} else {$pgnum = '1';}
    if($paged) {$paginated = ' Page '.$pgnum; } else {$paginated = '';}
    echo $thekw.' '.$paginated.': ';

    //Paged + Offsets
    $postperpage = get_option('posts_per_page');
    if($paged < 2 ) {
      $offset = '';
    } elseif($paged == 2) {
      $offset = $postperpage;
    } else {
      $offset = $postperpage*($paged-1);
    }
    $q = 'cat='. $catid.'&posts_per_page='.$limit.'&paged='.$paged.'&offset='.$offset;
    query_posts($q);
    if (have_posts()) : while (have_posts()) : the_post();
      $title = the_title('','',false);
      echo $title.', ';
    endwhile; endif; wp_reset_query();
    if ($keyword) {
      query_posts($q);
      if (have_posts()) : while (have_posts()) : the_post();
        $posttags = get_the_tags();
        if ($posttags) {
          shuffle($posttags);
          $i = 1;
          $tl = $limit + mt_rand(0,3);
          foreach($posttags as $tag) {
            if($i > $tl) {break;} else {
              $tagname = $tag->name; 
              $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
              echo ucwords($tagname).','; 
            }
            $i++;
          }
        }
      endwhile; endif; wp_reset_query();
    } else {}
    echo $thekw.'. ';

  // Tag Pages
  } elseif($where == 'tag') {
    $requri  = urldecode($_SERVER['REQUEST_URI']);
    $pgchk = js_ishere($requri, '/page/');
    if($pgchk) {
      $xrequri = explode('/page/',$requri);
      $siuri = $xrequri[0];
      $tagslug = basename($siuri);
    } else {
      $tagslug  = basename($requri);
    }
    //$tagslug = sanitize_title_with_dashes($terms);

    $isplus = js_ishere($terms, '+');
    if($isplus) {$thekw = str_replace('+',' ',$terms);} else {$thekw = $terms;}
    $thekw = $thekw; //<-------------

    //CLEAN UP : STARTS ----------------------------
    $rementity = array(
    '[&hellip;]', '&hellip;', '...', ' - ', '// ', ' http://', ' www.', '.com ', '.net ', '.org ', '.us ',
    '&#34;','&#39;','&#039;','&#8220;','&#8221;','&#8216;','&#8217;','&raquo;',
    '!&quot','&quot;','&lt;','&gt;','®',
    //Dont' edit this line below
    '#');
    $thekw = str_replace($rementity,' ',$thekw); 
    $thekw = wptexturize($thekw);
    $thekw = strtolower($thekw);
    $thekw = ucwords($thekw);
    //CLEAN UP : ENDS ----------------------------  
    $paged = get_query_var('paged');
    if($paged) {$pgnum = $paged;} else {$pgnum = '1';}
    if($paged) {$paginated = ' Page '.$pgnum; } else {$paginated = '';}
    echo $thekw.' '.$paginated.': ';

    //Paged + Offsets
    $postperpage = get_option('posts_per_page');
    if($paged < 2 ) {
      $offset = '';
    } elseif($paged == 2) {
      $offset = $postperpage;
    } else {
      $offset = $postperpage*($paged-1);
    }

    $q = 'tag='. $tagslug.'&posts_per_page='.$limit.'&paged='.$paged.'&offset='.$offset;
    query_posts($q);
    if (have_posts()) : while (have_posts()) : the_post();
     $title = the_title('','',false);
     echo $title.'. '; 
    endwhile; endif; wp_reset_query();
    if ($keyword) {
    query_posts($q);
     if (have_posts()) : while (have_posts()) : the_post();
      $posttags = get_the_tags();
      if ($posttags) { 
      shuffle($posttags);
      $i = 1;
      $tl = $limit + mt_rand(0,3);
      foreach($posttags as $tag) {
        if($i > $tl) {break;} else {
          $tagname = $tag->name; 
          $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
          echo ucwords($tagname).','; 
        }
        $i++;
      }}
     endwhile; endif; wp_reset_query();
    } else {}
    echo $thekw.'. ';

  // Daily Archive
  } elseif($where == 'isday') {
    $thisday = the_date('F jS, Y',false).' Archive:';
    $tday = the_date('j',false);
    $tmonth = the_date('n',false);
    $tyear = the_date('Y',false);
    $paged = get_query_var('paged');
    if($paged) {$pgnum = $paged;} else {$pgnum = '1';}
    if($paged) {$paginated = ' Page '.$pgnum.''; } else {$paginated = '';}
    echo $thisday.''.$paginated.': ';
    $q = 'year='. $tyear.'&monthnum='.$tmonth.'&day='.$tday.'&showposts='.$limit.'&paged='.$paged.'';
    //query_posts($q);
    if (have_posts()) : while (have_posts()) : the_post();
      $title = the_title('','',false);
      echo $title.'[.] ';
    endwhile; endif; wp_reset_query();
    if ($keyword) {
      query_posts($q);
      if (have_posts()) : while (have_posts()) : the_post();
        $posttags = get_the_tags();
        if ($posttags) {
          shuffle($posttags);
          $i = 1;
          $tl = $limit + mt_rand(0,3);
          foreach($posttags as $tag) {
            $tagname = $tag->name; 
            $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
            if($i > $tl) {break;} else {
              echo ucwords($tagname).','; 
            }
            $i++;
          }
        }
      endwhile; endif; wp_reset_query();
    } else {}

  // Monthly Archive
  } elseif($where == 'ismonth') {
    $thismonth = the_date('F, Y',false).' Archive';
    $tmonth = the_date('n',false);
    $tyear = the_date('Y',false);
    $paged = get_query_var('paged');
    if($paged) {$pgnum = $paged;} else {$pgnum = '1';}
    if($paged) {$paginated = ' Page '.$pgnum.''; } else {$paginated = '';}
    //Display it.
    echo $thismonth.''.$paginated.': ';
    $q = 'year='. $tyear.'&monthnum='.$tmonth.'&showposts='.$limit.'&paged='.$paged.'';
    //query_posts($q);
    if (have_posts()) : while (have_posts()) : the_post();
      $title = the_title('','',false);
      echo $title.'[.] ';
    endwhile; endif; wp_reset_query();
    if ($keyword) {
      //query_posts($q);
      if (have_posts()) : while (have_posts()) : the_post();
      $posttags = get_the_tags();
      if ($posttags) {
        shuffle($posttags);
        $i = 1;
        $tl = $limit + mt_rand(0,3);
        foreach($posttags as $tag) {
          if($i > $tl) {break;} else {
            $tagname = $tag->name; 
            $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
            echo ucwords($tagname).','; 
          }
          $i++;
        }
      }
      endwhile; endif; wp_reset_query();
    } else {}

  // Yearly Archive
  } elseif($where == 'isyear') {
    $tyear = the_date('Y');
    $paged = get_query_var('paged');
    echo 'Year '; the_time('Y'); echo' Archive: ';
    $q = 'year='. $tyear.'&showposts='.$limit.'&paged='.$paged.'';
    //query_posts($q);
    if (have_posts()) : 
      $ii = 1;
      while (have_posts()) : the_post();
        if($ii > $limit) {break;} else {
          $title = the_title('','',false);
          echo $title.'[.] ';
        }
        $ii++;
      endwhile; 
    endif; //wp_reset_query();
    if ($keyword) {
      //query_posts($q);
      if (have_posts()) : 
        $ui = 1;
        while (have_posts()) : the_post();
          if($ui > $limit) {break;} else {
            $posttags = get_the_tags();
            if ($posttags) {
              shuffle($posttags);
              $i = 1;
              $tl = $limit - mt_rand(0,1);
              foreach($posttags as $tag) {
                if($i > $tl) {break;} else {
                  $tagname = $tag->name; 
                  $tagname = htmlentities($tagname, ENT_QUOTES, "UTF-8");
                  echo ucwords($tagname).','; 
                }
                $i++;
              }
            }
          }
          $ui++;
        endwhile; endif; //wp_reset_query();
    } else {}
    echo $thekw.'. ';
  } else {}
}

/**
* USAGE EXAMPLES
*/
/*
<?php if (is_home() || is_attachment() || is_single() || is_page()) { ?>
<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
<?php } else {} ?>

<?php wp_head(); ?>

<?php if (is_home() || is_attachment() || is_single() || is_page()) {} else { ?>
<title><?php 
// Search Title
if(is_search()) {
 $theqs = str_replace(' ', '+', str_replace('%20', '+', get_query_var('s'))); 
 js_seo($s, 'search','2',false);

//Category Page
} elseif(is_category()) {
  $thecat = single_cat_title('',false);
  js_seo($thecat, 'category','3',false);

//Tag Pages
} elseif(is_tag()) {
  $thetag = single_tag_title('',false);
  js_seo($thetag, 'tag','3',false);

//Archive Pages
} elseif(is_archive()) {
  //Daily Archive Pages
  if(is_day()) {
    js_seo('', 'isday','3',false);

  //Monthly Archive Pages
  } elseif(is_month()) {
    $thismonth = the_time('F, Y').' Archive';
    js_seo('', 'ismonth','3',false);

  //Yearly Archive Pages
  } elseif(is_year()) {
    js_seo('', 'isyear','3',false);
  }

} // Back to Original WP Title
?></title>
<?php } ?>

*/

/**
** Meta Description + Meta Keywords for Search, Category, Archive and Tag pages.
**/
//Search Page
/*
if (is_search()) {
 $theqs = str_replace(' ', '+', str_replace('%20', '+', get_query_var('s'))); 
?><?php
?><meta name="description" content="<?php js_seo($theqs, 'search','4',true); ?>" /><?php
?><meta name="keywords" content="<?php js_seo($theqs, 'search','4',true); ?>" /><?php
?><?php //Category Page
} elseif(is_category()) {
  $thecat = single_cat_title('',false);
?><?php
?><meta name="description" content="<?php js_seo($thecat, 'category','5',true); ?>" /><?php
?><meta name="keywords" content="<?php js_seo($thecat, 'category','5',true); ?>" /><?php
?><?php //Category Page
} elseif(is_tag()) {
  $thetag = single_tag_title('',false);
?><meta name="description" content="<?php js_seo($thetag, 'tag','5',true); ?>" /><?php
?><meta name="keywords" content="<?php js_seo($thetag, 'tag','5',true); ?>" /><?php
?><?php //Archive Page
} elseif(is_archive()) {
  //Daily Archive
  if(is_day()) { ?><?php
?><meta name="description" content="<?php js_seo('', 'isday','5',true); ?>" /><?php
?><meta name="keywords" content="<?php the_time('F jS, Y'); ?><?php js_seo('', 'isday','5',true); ?>" /><?php
?><?php //Monthly Archive
  } elseif(is_month()) { ?><?php
?><meta name="description" content="<?php js_seo($thismonth, 'ismonth','5',true); ?>" /><?php
?><meta name="keywords" content="<?php the_time('F, Y'); ?><?php js_seo($thismonth, 'ismonth','5',true); ?>" /><?php
?><?php //Yearly Archive
  } elseif(is_year()) { ?><?php
?><meta name="description" content="<?php js_seo('', 'isyear','5',true); ?>" /><?php
?><meta name="keywords" content="<?php js_seo('', 'isyear','5',true); ?>" /><?php 
  } else {}

//Attachment Page
} elseif(is_attachment()) { ?><?php 
?><meta name="description" content="<?php
$deattid = $post->ID; 
$dpost = get_post($deattid);
$dtitle = $dpost->post_title;
$dcontent = $dpost->post_content;
$dcontent = ucfirst(strtolower($dcontent));

//We have Parent's post ID
$prid = $dpost->post_parent;
$parr = get_post($prid);

$prtitle = $parr->post_title;
$posttags = get_the_tags($prid);
$dtags = '';
if ($posttags) {
  foreach($posttags as $tag) {
    $tagname = $tag->name;
    $dtags .= $tagname.','; 
  }
}
$dtags .= ']';
//We have the Tags
$detags = str_replace(',]','',$dtags);
//Construct Meta Desc.
$mtdesc = $prtitle.' : '.$dtitle.'. '.$dcontent.'. '.$detags;
echo $mtdesc;
?>" />

*/

/* JS Is Here 
** Needle finder, works for both single needle or arrays of needles.
$hastack : judul
$needle : array kwd,
Version: 2.0
Date: June 02, 2012
*/
function js_ishere($haystack, $needle) {
  if(!is_array($needle)) $needle = array($needle);
  foreach($needle as $what) {
    $what = strtolower($what);
    $pos = strpos($haystack, $what);
    if($pos !==false) 
    return true;
  }
  return false;
}


?>
<?php

/**
* Add rel=nofollow to wp_list_categories
* @link http://www.billerickson.net/adding-nofollow-to-category-links/
* @author Bill Erickson
*/
function add_nofollow( $text ) {
$text = stripslashes($text);
$text = preg_replace_callback('|<a (.+?)>|i', 'wp_rel_nofollow_callback', $text);
return $text;
}
add_filter( 'wp_list_categories', 'add_nofollow' );

/**
* Add rel=nofollow to the_category
* @link http://www.billerickson.net/adding-nofollow-to-category-links/
* @author Bill Erickson
*/
function add_nofollow_cat( $text ) {
$text = str_replace('rel="category tag"', "", $text);
$text = add_nofollow($text);
return $text;
}
add_filter( 'the_category', 'add_nofollow_cat' );
?>
<?php
function custom_tag_cloud( $args = '' ) {
        $defaults = array(
                'smallest' => 11, 'largest' => 11, 'unit' => 'px', 'number' => 35,
                'format' => 'flat', 'separator' => "\n", 'orderby' => 'count', 'order' => 'RAND',
                'exclude' => '', 'include' => '', 'link' => 'view', 'taxonomy' => 'post_tag', 'echo' => true
        );
        $args = wp_parse_args( $args, $defaults );
 
        $tags = get_terms( $args['taxonomy'], array_merge( $args, array( 'orderby' => 'count', 'order' => 'DESC' ) ) ); // Always query top tags
 
        if ( empty( $tags ) )
                return;
 
        foreach ( $tags as $key => $tag ) {
                if ( 'edit' == $args['link'] )
                        $link = get_edit_tag_link( $tag->term_id, $args['taxonomy'] );
                else
                        $link = get_term_link( intval($tag->term_id), $args['taxonomy'] );
                if ( is_wp_error( $link ) )
                        return false;
 
                $tags[ $key ]->link = $link;
                $tags[ $key ]->id = $tag->term_id;
        }
 
        $return = wp_generate_tag_cloud( $tags, $args ); // Here's where those top tags get sorted according to $args
 
        $return = apply_filters( 'custom_tag_cloud', $return, $args );
 
        if ( 'array' == $args['format'] || empty($args['echo']) )
                return $return;
 
        echo $return;
}
remove_filter('wp_tag_cloud', 'custom_tag_cloud');
add_filter('wp_tag_cloud', 'custom_tag_cloud','add_nofollow');
add_filter( 'wp_generate_tag_cloud', 'my_nofollow_tag_cloud_example' );
function my_nofollow_tag_cloud_example( $text ) {
   return preg_replace_callback('|<a (.+?)>|i', 'wp_rel_nofollow_callback', $text);
}
?>
<?php
/*sgl tags */
function sgl_tags () {
 global $post;	
 if(is_attachment()) {
	$postid = $post->post_parent;
 } else {
	$postid = $post->ID;
 }
 //tags
 $tags = get_the_tags($postid);
 if($tags) {
	$i = 1;
	$limit = 5;
	$nw = 'rel="nofollow"';
	shuffle($tags);
	echo 'Tags: ';
	foreach($tags as $tag) {
		if($i > $limit) {
		  break;
		} else {
		$tagname = $tag->name;
		$taglink = get_tag_link($tag->term_id);
		if($i == $limit) { $comma = '';} else { $comma = ',';}
		echo '<a '.$nw.' href="'.$taglink.'">'.$tagname.'</a>'.$comma.' ';
		}
		$i++;
	
	}
 
 }
 
}
// End of sgl tags ---------------------------------------------------
/* Singlecrumbs */
function singlecrumbs() {
  $delimiter = '&raquo;';
  $name = 'Home';
  $vw = 'Curently View';
  $currentBefore = '<li>';
  $currentAfter = '</li>';
 
  if ( !is_home() && !is_front_page() || is_paged() ) {
 	 echo '<ul id="crumbs">';
    global $post;
    $home = get_bloginfo('url');
    echo '<li><a href="' . $home . '">' . $name . '</a> </li>';
 
    if ( is_category() ) {
      global $wp_query;
      $cat_obj = $wp_query->get_queried_object();
      $thisCat = $cat_obj->term_id;
      $thisCat = get_category($thisCat);
      $parentCat = get_category($thisCat->parent);
      if ($thisCat->parent != 0) echo(get_category_parents($parentCat, TRUE, ' '));
      echo $currentBefore . '';
      single_cat_title();
      echo '' . $currentAfter;
 
    } elseif ( is_day() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> </li>';
      echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> </li>' ;
      echo $currentBefore . get_the_time('d') . $currentAfter;
 
    } elseif ( is_month() ) {
      echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>  </li>';
      echo $currentBefore . get_the_time('F') . $currentAfter;
 
    } elseif ( is_year() ) {
      echo $currentBefore . get_the_time('Y') . $currentAfter;
 
    } elseif ( is_single() && !is_attachment() ) {
      $cat = get_the_category(); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, '');
	echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0]; echo $currentBefore;
      echo get_category_parents($cat, TRUE, ' ');echo $currentAfter;
      echo '<li><a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a> </li>';
      echo $currentBefore;
      echo $vw;
      echo $currentAfter;
 
    } elseif ( is_page() && !$post->post_parent ) {
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<li><a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a></li>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      foreach ($breadcrumbs as $crumb) echo $crumb . ' ';
      echo $currentBefore;
      the_title();
      echo $currentAfter;
 
    } elseif ( is_search() ) {
      echo $currentBefore . '' . get_search_query() . '' . $currentAfter;
 
    } elseif ( is_tag() ) {
      echo $currentBefore . 'Posts tagged &#39;';
      single_tag_title();
      echo '&#39;' . $currentAfter;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $currentBefore . 'Articles posted by ' . $userdata->display_name . $currentAfter;
 
    } elseif ( is_404() ) {
      echo $currentBefore . 'Error 404' . $currentAfter;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __(' Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
  }
echo '</ul>';
}
/*main tags */
function main_tags () {
 global $post;	
 if(is_attachment()) {
	$postid = $post->post_parent;
 } else {
	$postid = $post->ID;
 }
 //tags
 $tags = get_the_tags($postid);
 if($tags) {
	$i = 1;
	$limit = 5;
	$nw = 'rel="nofollow"';
	shuffle($tags);
	echo 'Tags: ';
	foreach($tags as $tag) {
		if($i > $limit) {
		  break;
		} else {
		$tagname = $tag->name;
		$taglink = get_tag_link($tag->term_id);
		if($i == $limit) { $comma = '';} else { $comma = ',';}
		echo '<a '.$nw.' href="'.$taglink.'">'.$tagname.'</a>'.$comma.' ';
		}
		$i++;
	
	}
 
 }
 
}
// End of main tags ---------------------------------------------------
/*---------------CUSTOM IMG SINGLE ----------*/
function gallerysingle($args = array())
{
	
	$get_posts_args = array(
	"post_parent"    => get_the_ID(),
	"what_to_show"=>"posts",

	"post_type"=>"attachment",
	"orderby"=>"RAND",
	"order"=>"RAND",
	"showposts"=>20,
	"post_mime_type"=>"image/jpeg,image/jpg,image/gif,image/png");
	$posts = get_posts($get_posts_args);
	foreach ($posts as $post)
	{
		$parent = get_post($post->post_parent);
		if(($imgsrc = wp_get_attachment_image($post->ID,'thumb')) 
				&& ($imglink= get_attachment_link($post->ID))
				&& $parent->post_status == "publish")
		{
			echo  "<a href='" . $imglink . "'>".$imgsrc."</a>" ;
		}
	}
}
/*---------------END IMG SINGLE ----------*/
/**~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
** JS NAVI
**/
function js_navi() { // pages will be show before and after current page
//if (is_single() && is_page()) return; // don't show in single page
global $wp_query, $paged;
$max_page = $wp_query->max_num_pages;
$p = $max_page;
if ($max_page == 1 || $max_page == 0 ) return; // don't show when only one page
echo '<div class="jsnavi">';
if (empty($paged)) $paged = 1;
//echo '<span class="pages">Page ' . $paged . ' of ' . $max_page . '</span>'; // pages
if ( $paged > $p + 1 ) p_link( 1, '&laquo; First' );
if ( $paged > $p + 2 ) echo '<span>...</span>';
if ( $paged > $p + 2 ) echo "<a href='", esc_html( get_pagenum_link( $paged-1 ) ), "' title='Previous'>&laquo;</a>";
for( $i = $paged - $p; $i <= $paged + $p; $i++ ) { // Middle pages
if($i < 10) {$current = zeroise($i,3);} else {$current = zeroise($i,3);}
if ( $i > 0 && $i <= $max_page ) $i == $paged ? print "<span class='current'>&#8226</span>" : p_link( $i,$i );
}
if ( $paged < $max_page - $p - 1 ) echo "<a href='", esc_html( get_pagenum_link( $paged+1 ) ), "' title='Next'>&raquo;</a>";
if ( $paged < $max_page - $p - 1 ) echo '<span>...</span>';
if ( $paged < $max_page - $p ) p_link( $max_page, 'Last &raquo;' );
echo'<p class="clear lh0 don0"></p></div>';
}
function p_link( $i, $title = '' ) {
if ($title == '') $title = "Page {$i}";
if($title < 10) {$title = zeroise($title,3);} else {$title = zeroise($title,3);}
echo "<a href='", esc_html(get_pagenum_link($i)), "' title='Page {$i}'>&#8226</a>";
}
/*---------------CUSTOM IMG SIDEBAR ----------*/
function sidebarimg($args = array())
{
	
	$get_posts_args = array(
	"post_parent"    => get_the_ID(),
	"what_to_show"=>"posts",

	"post_type"=>"attachment",
	"orderby"=>"RAND",
	"order"=>"RAND",
	"showposts"=>1,
	"post_mime_type"=>"image/jpeg,image/jpg,image/gif,image/png");
	$nofollow = 'rel="nofollow"';
	$posts = get_posts($get_posts_args);
	foreach ($posts as $post)
	{
		$parent = get_post($post->post_parent);
		if(($imgsrc = wp_get_attachment_image($post->ID,'thumb')) 
				&& ($imglink= get_attachment_link($post->ID))
				&& $parent->post_status == "publish")
		{
			echo  "<a href='" . $imglink . "'>".$imgsrc."</a>" ;
			echo '</span>';
			echo '<em>';
			echo apply_filters( 'the_title' , $post->post_title );
			echo '</em>';
		}
	}
}
/*---------------end IMG SIDEBAR ----------*/
/*---------------CUSTOM IMGmain ----------*/
function randimgn($args = array()) {
	global $wpdb;
	$get_posts_args = array(
	"post_parent"    => get_the_ID(),
	"what_to_show"=>"posts",
	"post_status"=> null,
	"post_type"=>"attachment",
    	'orderby' => 'rand',  
	"showposts"=>1,
	"post_mime_type"=>"image/jpeg,image/jpg,image/gif,image/png");
	$nofollow = 'rel="nofollow"';
	$posts = get_posts($get_posts_args);
	foreach ($posts as $post)
	{
		$parent = get_post($post->post_parent);
		if(($imgsrc = wp_get_attachment_image($post->ID, 'medium')) 
				&& ($imglink= get_attachment_link($post->ID))
				&& $parent->post_status == "publish")
		{
			echo  "<a href='" . $imglink . "'>".$imgsrc."</a>";
		}
	}
}
/*---------------end IMGmain ----------*/
/** JS SPIN -----------------------------------------------------
* Simple spinner
*/
function js_spin($s){
$s = str_replace(array('{','}'),'',$s);
$e = explode("|", $s);
$c = count($e);
$l = $c-1;
$i = mt_rand(0,$l);
$t = $e[$i];
return $t;
}
/*---------------CUSTOM IMG RELATED ----------*/
function relateds($args = array())
{
	
	$get_posts_args = array(
	"post_parent"    => get_the_ID(),
	"what_to_show"=>"posts",

	"post_type"=>"attachment",
	"orderby"=>"RAND",
	"order"=>"RAND",
	"showposts"=>1,
	"post_mime_type"=>"image/jpeg,image/jpg,image/gif,image/png");
	$posts = get_posts($get_posts_args);
	foreach ($posts as $post)
	{
		$parent = get_post($post->post_parent);
		if(($imgsrc = wp_get_attachment_image($post->ID, 'thumb')) 
				&& ($imglink= get_attachment_link($post->ID))
				&& $parent->post_status == "publish")
		{
			echo  "<a href='" . $imglink . "'>".$imgsrc."</a>" ;
		}
	}
}
/*---------------END CUSTOM IMG RELATED ----------*/
/*---------------CUSTOM IMG ATT ----------*/
function gb() {
	global $post;
	$pos = get_post($post);
	if ( $gb = get_children(array(
					'post_parent' => $pos->post_parent,
					'post_type' => 'attachment',
					'numberposts' => 50, 
					'post_mime_type' => 'image',)))
	{
		foreach( $gb as $gbr ) {
			$url=get_attachment_link($gbr->ID);
			$gambar=wp_get_attachment_image( $gbr->ID, 'thumb');
			echo '<a href="'.$url.'">'.$gambar.'</a>';
		}
	}
}
/*---------------END CUSTOM IMG ATT ----------*/
/*---------------CUSTOM SUMMARY ----------*/
function sum($limit) {
	$sum = explode(' ', get_the_excerpt(), $limit);
	if (count($sum)>=$limit) {
		array_pop($sum);
		$sum = implode(" ",$sum).'...';
	} else {
		$sum = implode(" ",$sum);
	}	
	$sum = preg_replace('`\[[^\]]*\]`','',$sum);
	return $sum;
}
/*---------------END CUSTOM SUMMARY ----------*/

/*---------------CUSTOM EXCERPTS ----------*/
function an_excerptlength_archive($length) {
    return 60;
}
function an_excerptlength_index($length) {
    return 70;
}
function an_excerptlength_related($length) {
    return 60;
}

function an_excerpt($length_callback='', $more_callback='') {
    global $post;
    if(function_exists($length_callback)){
        add_filter('excerpt_length', $length_callback);
    }
    if(function_exists($more_callback)){
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>'.$output.'</p>';
    echo $output;
}
/*------------------- END CUSTOM EXCERPTS ---------------------*/
// Filter to add nofollow attribute -----------------
function nofollow_archives($link_html) {
    return str_replace('<a href=', '<a rel="nofollow" href=',  $link_html); 
}
// Create my archives widget
function my_archives_widget() { ?>
<li class="widget">
        <h3>My Archives</h3>
        <div class="widget widget_text"><ul>
        <?php add_filter('get_archives_link', 'nofollow_archives');  
        get_archives('postbypost', '3', 'html', '<li class="page_item">','</li>', FALSE);
        remove_filter('get_archives_link', 'nofollow_archives'); ?>
    </ul></div>
</li>
<?php
}
add_action('widgets_init', 'my_archives_widget_init');
// Register my archives widget
function my_archives_widget_init() {
  register_sidebar_widget('My Archives Widget', 'my_archives_widget');
}

add_custom_background();
if ( function_exists('register_sidebar') ) register_sidebars(1); 

add_action('admin_menu', 'business_theme_page');

function business_theme_page ()
{
	if ( count($_POST) > 0 && isset($_POST['business_settings']) )
	{
		$options = array ('ads-1', 'ads-2', 'ads-3', 'ads-4', 'ads-6', 'ads-5', 'header', 'analytics');
		
		foreach ( $options as $opt )
		{
			delete_option ( 'business_'.$opt, $_POST[$opt] );
			add_option ( 'business_'.$opt, $_POST[$opt] );	
		}			
		
		 
	}
	add_theme_page(__('Wp Business Theme Options'), __('Wp Business Theme Options'), 'edit_themes', basename(__FILE__), 'business_settings');	
}
function business_settings ()
{?>
<div class="wrap">
	<h3>Wp Business Theme Options Panel</h3>
	
<form method="post" action="">
	<table class="form-table">
		<!-- General settings -->
		<tr>
			<th colspan="2"><strong>General Settings</strong></th>
		</tr>	
		<!-- Header Script -->
		<tr valign="top">
			<th scope="row"><label for="ads-1">468x60 Header</label></th>
			<td>
				<textarea cols="60" rows="5" name="ads-1" id="ads-1" /><?php echo stripslashes(get_option('business_ads-1')); ?></textarea>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="ads-2">160x600 Sidebar</label></th>
			<td>
				<textarea cols="60" rows="5" name="ads-2" id="ads-2" /><?php echo stripslashes(get_option('business_ads-2')); ?></textarea>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="ads-3">728x90 after Title Att</label></th>
			<td>
				<textarea cols="60" rows="5" name="ads-3" id="ads-3" /><?php echo stripslashes(get_option('business_ads-3')); ?></textarea>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="ads-4">468x60 Above title of first post</label></th>
			<td>
				<textarea cols="60" rows="5" name="ads-4" id="ads-4" /><?php echo stripslashes(get_option('business_ads-4')); ?></textarea>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="ads-6">468x60 After 2 Post in Homepage</label></th>
			<td>
				<textarea cols="60" rows="6" name="ads-6" id="ads-6" /><?php echo stripslashes(get_option('business_ads-6')); ?></textarea>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row"><label for="ads-5">Floating Ads</label></th>
			<td>
				<textarea cols="60" rows="5" name="ads-5" id="ads-5" /><?php echo stripslashes(get_option('business_ads-5')); ?></textarea>
			</td>
		</tr>
		<tr>
			<th colspan="2"><strong>Header Script</strong></th>
		</tr>
			<tr valign="top">
			<th scope="row"><label for="header">Header Scripts</label></th>
			<td>
				<textarea cols="60" rows="5" name="header" id="header" /><?php echo stripslashes(get_option('business_header')); ?></textarea>
			<br /><em>If you need to add scripts to your header (like meta tag verification, perhaps), <br />you should enter them in the box below. They will be added before &lt;/head&gt; tag</em>
			</td>
		</tr>
		<tr>
			<th colspan="2"><strong>Google Analytics</strong></th>
		</tr>
		<tr>
			<th><label for="ads">Google Analytics code:</label></th>
			<td>
				<textarea name="analytics" id="analytics" rows="7" cols="70" style="font-size:11px;"><?php echo stripslashes(get_option('business_analytics')); ?></textarea>
			</td>
		</tr>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="Save Changes" />
		<input type="hidden" name="business_settings" value="save" style="display:none;" />
	</p>
</form>

</div>
<?php }?>
<?php
    if ( ! function_exists( 'breadcrumbs' ) ) :
    function breadcrumbs() {
    $separator = '&rsaquo;';
    $home = 'Home';
    $cv = 'Curently View';
    echo '<ul xmlns:v="http://rdf.data-vocabulary.org/#" id="crumbs">';
    global $post;
    echo '  <li typeof="v:Breadcrumb">
    <a rel="v:url" property="v:title" href="'.home_url( '/' ).'">'.$home.'</a>
    </li> ';	
    $category = get_the_category();
    if ($category) {
    foreach($category as $category) {
    echo  "<li typeof=\"v:Breadcrumb\">
    <a rel=\"v:url\" property=\"v:title\" href=\"".get_category_link($category->term_id)."\" >$category->name</a> 
    </li>";
	$cv;
    }
    }
    echo '</ul>';
    }
    endif;
    ?>
<?php

/* List with IP ranges. Use the '*' as the range selector */
$ban_ip_range = array('72.18.192.*', '72.18.193.*', '72.18.194.*', '72.18.195.*', '72.18.196.*', '72.18.197.*', '72.18.198.*', '72.18.199.*', '72.18.200.*', '72.18.201.*', '72.18.202.*', '72.18.203.*', '72.18.204.*', '72.18.205.*', '72.18.206.*', '72.18.207.*', '111.118.189.*', '23.19.*.*', '174.142.*.*', '107.6.124.*', '67.230.166.*', '109.169.56.*', '109.169.57.*', '74.207.224.*', '74.207.225.*', '74.207.226.*', '74.207.227.*', '74.207.228.*', '74.207.229.*', '74.207.230.*', '74.207.231.*', '74.207.232.*', '74.207.233.*', '74.207.234.*', '74.207.235.*', '74.207.236.*', '74.207.237.*', '74.207.238.*', '74.207.239.*', '74.207.240.*', '74.207.241.*', '74.207.242.*', '74.207.243.*', '74.207.244.*', '74.207.245.*', '74.207.246.*', '74.207.247.*', '74.207.248.*', '74.207.249.*', '74.207.250.*', '74.207.251.*', '74.207.252.*', '74.207.253.*', '74.207.254.*', '74.207.255.*', '172.128.*.*', '172.129.*.*', '172.130.*.*', '172.131.*.*', '172.132.*.*', '172.133.*.*', '172.134.*.*', '172.135.*.*', '172.136.*.*', '172.137.*.*','172.138.*.*','172.139.*.*', '172.140.*.*', '172.141.*.*', '172.142.*.*', '172.143.*.*', '172.144.*.*', '172.145.*.*', '172.146.*.*', '172.147.*.*', '172.148.*.*', '172.149.*.*', '172.150.*.*', '172.151.*.*', '172.152.*.*', '172.153.*.*', '172.154.*.*', '172.155.*.*', '172.156.*.*', '172.157.*.*', '172.158.*.*', '172.159.*.*', '172.160.*.*', '172.161.*.*', '172.162.*.*', '172.163.*.*', '172.164.*.*', '172.165.*.*', '172.166.*.*', '172.167.*.*', '172.168.*.*', '172.169.*.*', '172.170.*.*', '172.171.*.*', '172.172.*.*', '172.173.*.*', '172.174.*.*', '172.175.*.*', '172.176.*.*', '172.177.*.*', '172.178.*.*', '172.179.*.*', '172.180.*.*', '172.181.*.*', '172.182.*.*', '172.183.*.*', '172.184.*.*', '172.185.*.*', '172.186.*.*', '172.187.*.*', '172.188.*.*', '172.189.*.*', '172.190.*.*', '172.191.*.*', '173.254.192.*', '173.254.193.*', '173.254.194.*', '173.254.195.*', '173.254.196.*', '173.254.197.*', '173.254.198.*', '173.254.199.*', '173.254.201.*', '173.254.202.*', '173.254.203.*', '173.254.204.*', '173.254.205.*', 
'173.254.207.*', '173.254.208.*', '173.254.209.*','173.254.210.*', '173.254.211.*', '173.254.212.*', '173.254.213.*', '173.254.214.*', '173.254.215.*', '173.254.216.*', '173.254.217.*', '173.254.218.*', '173.254.219.*', '173.254.233.*', '173.254.220.*', '173.254.221.*', '173.254.222.*', '173.254.223.*', '173.254.224.*', '173.254.225.*', '173.254.232.*', '173.254.226.*', '173.254.227.*', '173.254.228.*', '173.254.229.*', '173.254.230.*', '173.254.231.*', '173.254.234.*', '173.254.235.*', '173.254.236.*', '173.254.237.*', '173.254.238.*', '173.254.239.*', '173.254.240.*', '173.254.241.*', '173.254.242.*', '173.254.243.*', '173.254.244.*', '173.254.245.*', '173.254.246.*', '173.254.247.*', '173.254.248.*', '173.254.249.*', '173.254.250.*', '173.254.251.*', '173.254.252.*', '173.254.253.*', '173.254.254.*', '173.254.255.*', '173.254.206.*'
);

/* Visitor's IP Address */
$user_ip = $_SERVER['REMOTE_ADDR'];

/* Message to output if the IP is in the ban list */
$msg = '
<style type="text/css">
/* Style warning button to look like a small text link in the
         bottom right. This is preferable to just using a text link
         since there is already a mechanism in browser.js for trapping
         oncommand events from unprivileged chrome pages (BrowserOnCommand).*/

#bdy {background: #424242}
#bdt {height:350px;width:800px;background:#e4e4e4;border:1px solid #646563;border-radius:10px;box-shadow:0 0 6px #0f0f0f;margin: 100px auto;padding:30px 0 0 0}
#sgtxt {font-size:50px;margin:20px auto;text-align:center;transform:rotate(0deg);-webkit-transform: rotate(0deg); -moz-transform: rotate(0deg); -o-transform: rotate(0deg);text-shadow: 0 1px #fff;font-family: laconic, sans-serif;color:#646563;}
#sgtxst {font-size:30px;margin:20px auto;text-align:center;text-shadow: 0 1px #fff;color:#646563;}
#bdtt {height:150px;width:700px;background:#999;border:1px solid #555;border-radius:10px;box-shadow: inset 0 0 6px #333;margin: 0 auto;}
#sgtxct {font-size:14px;margin:0px auto;text-align:center;text-shadow: 0 1px #ccc;color: #4d4d4d}
#sgtxctt {font-size:30px;margin:0px auto;text-align:center;text-shadow: 0 1px #ccc;color:#0d7700;font-style:italic}
#bctc {height:15px;width:600px;background: #187a2d
background: -moz-linear-gradient(left,  #187a2d 0%, #23a51a 16%, #51c124 22%, #ffffff 22%, #f2f2f2 100%);
background: -webkit-gradient(linear, left top, right top, color-stop(0%,#187a2d), color-stop(16%,#23a51a), color-stop(22%,#51c124), color-stop(22%,#ffffff), color-stop(100%,#f2f2f2));
background: -webkit-linear-gradient(left,  #187a2d 0%,#23a51a 16%,#51c124 22%,#ffffff 22%,#f2f2f2 100%);
background: -o-linear-gradient(left,  #187a2d 0%,#23a51a 16%,#51c124 22%,#ffffff 22%,#f2f2f2 100%);
background: -ms-linear-gradient(left,  #187a2d 0%,#23a51a 16%,#51c124 22%,#ffffff 22%,#f2f2f2 100%);
background: linear-gradient(to right,  #187a2d 0%,#23a51a 16%,#51c124 22%,#ffffff 22%,#f2f2f2 100%);
;border:1px solid #0f0f0f;border-radius:10px;box-shadow:0 0 6px #0f0f0f;margin: 20px auto;}
#stxst {font-size:12px;margin:-25px 50px 0 ;text-align:right;text-shadow: 0 1px #ccc;color: #4d4d4d;font-style:italic}
#st12 {color:#0d7700}
</style>


<body id="bdy">
<div id="bdt">
<div id="sgtxt">Castlives.com</div>
<div style="clear: both"></div>
<div id="sgtxst"><p>Sorry, Page under Construction!<p></div>
<div id="bdtt">
<div id="sgtxct"><p>We are working on the database</p></div>
<div id="sgtxctt"><p>Please Visit Us Later</p></div>
<div id="bctc"></div>
<div id="stxst"><p>We are about <span id="st12">27%</span> finished</p></div>
</div>
</div>
</div>
';

/* Check if the Visitor's IP is in our range's list */

if(!empty($ban_ip_range))
{
foreach($ban_ip_range as $range)
{
	$range = str_replace('*','(.*)', $range);

    if(preg_match('/'.$range.'/', $user_ip))
	{
	  exit($msg);
	}
}
}
?>
<?php 
// iblocked -----------------------------------------
function getUserIP()
{
    //check ip from share internet
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    //to check ip is pass from proxy
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
// The blacklisted ips.
$cd = getUserIP();
$iblocked = ag_ipilter();
function ag_ipilter() {
global $cd,$current_user;
get_currentuserinfo();
$uid = $current_user->ID;
$u = new WP_User($uid);
if (!empty($u->roles) && is_array($u->roles)) { $ur = ''; foreach ($u->roles as $r) {$ur .= $r.'';} $ur .= '';}
if ($cd == '69.112.160.231' || $cd == '173.244.217.155' || $cd == '174.103.185.130' || $cd == '50.79.246.246' || 
$cd == '24.5.29.213' || $cd == '1.225.252.175' || $cd == '62.68.59.131' || $cd == '206.109.103.215' || $cd == '69.243.182.60' || 
$cd == '74.132.245.63' || $cd == '96.57.97.170' || $cd == '72.218.34.24' || $cd == '98.89.23.102' || $cd == '98.86.214.225' || 
$cd == '216.217.181.46' || $cd == '217.165.161.177' || $cd == '173.221.104.90' || $cd == '196.20.2.3' || $cd == '208.96.92.237' || 
$cd == '41.232.169.219' || $cd == '41.35.177.58' || $cd == '184.12.237.60' || $cd == '82.150.102.2' || $cd == '108.85.239.36' || 
$cd == '195.229.241.171' || $cd == '71.49.80.137' || $cd == '96.32.5.198' || $cd == '207.200.116.131' || $cd == '136.165.142.232' || 
$cd == '196.34.16.68' || $cd == '63.97.155.233' || $cd == '83.101.87.40' || $cd == '46.184.255.88' || $cd == '76.30.172.11' || 
$cd == '162.95.148.249' || $cd == '164.116.84.113' || $cd == '97.127.61.25' || $cd == '99.254.74.227' || $cd == '184.96.152.238' || 
$cd == '81.44.73.21' || $cd == '68.9.168.78' || $cd == '67.244.119.241' || $cd == '75.108.147.64' || $cd == '173.27.219.23' || 
$cd == '184.189.64.60' || $cd == '71.227.86.123' || $cd == '74.3.160.86' || $cd == '174.142.4.54' || $cd == '66.90.101.217' || 
$cd == '66.90.73.223' || $cd == '95.154.230.191' || $cd == '67.159.5.242' || $cd == '93.174.93.145' || $cd == '74.207.225.162' || 
$cd == '184.82.241.228' || $cd == '193.200.150.152' || $cd == '204.45.133.74' || $cd == '217.165.161.177' || $cd == '173.221.104.90' || 
$cd == '196.20.2.3' || $cd == '208.96.92.237' || $cd == '41.232.169.219' || $cd == '41.35.177.58' || $cd == '24.5.29.213' || 
$cd == '50.79.246.246' || $cd == '108.85.239.36' || $cd == '195.229.241.171' || $cd == '71.49.80.137' || $cd == '207.200.116.131' || 
$cd == '136.165.142.232' || $cd == '63.97.155.233' || $cd == '46.184.255.88' || $cd == '67.230.166.52' || $cd == '217.16.75.19' || 
$cd == '217.16.229.41' || $cd == '164.116.84.113' || $cd == '184.96.152.238' || $cd == '81.44.73.21' || $cd == '68.9.168.78' || 
$cd == '67.244.119.241' || $cd == '75.108.147.64' || $cd == '173.27.219.23' || $cd == '184.189.64.60' || $cd == '24.5.29.213' || 
$cd == '50.79.246.246' || $cd == '82.150.102.2' || $cd == '108.85.239.36' || $cd == '195.229.241.171' || $cd == '71.49.80.137' || 
$cd == '194.25.30.14' || $cd == '136.165.142.232' || $cd == '63.97.155.233' || $cd == '46.184.255.88' || $cd == '164.116.84.113' || 
$cd == '184.96.152.238' || $cd == '81.44.73.21' || $cd == '68.9.168.78' || $cd == '75.108.147.64' || $cd == '173.27.219.23' || 
$cd == '184.189.64.60' || $cd == '72.135.57.99' || $cd == '77.42.157.146' || $cd == '155.150.223.145' || $cd == '167.135.14.181' || 
$cd == '210.242.215.197' || $cd == '210.242.215.234' || $cd == '210.242.215.239' || $cd == '64.12.116.135' || $cd == '64.12.116.6' || 
$cd == '64.12.116.70'  || $cd == '64.12.116.71' || $cd == '71.53.65.199' || $cd == '101.173.127.228' || $cd == '14.219.26.213' || 
$cd == '24.48.133.59' || $cd == '63.120.127.196' || $cd == '98.227.147.73' || $cd == '12.68.136.161' || $cd == '182.14.201.101' || 
$cd == '81.183.110.195' || $cd == '108.66.100.122' || $cd == '129.113.52.188' || $cd == '209.240.60.31' ||$cd == '50.4.136.73' || 
$cd == '66.185.213.229' || $cd == '76.221.201.189' || $cd == '178.128.93.239' || $cd == '93.43.137.2' || $cd == '173.51.38.222' || 
$cd == '68.172.227.201' || $cd == '108.17.7.14' || $cd == '98.194.70.108' || $cd == '207.126.177.38' || $cd == '99.189.21.226' || 
$cd == '64.12.116.70' || $cd == '64.12.116.135' || $cd == '64.12.116.6' || $cd == '64.12.116.71' || $cd == '70.120.220.146' || 
$cd == '152.133.7.134' || $cd == '158.48.6.140' || $cd == '65.244.99.243' || $cd == '64.30.110.35' || $cd == '141.237.10.167' || 
$cd == '68.61.228.179' || $cd == '24.155.138.107' || $cd == '184.155.184.47' || $cd == '161.141.1.1' || $cd == '184.99.243.119' || 
$cd == '75.83.236.66' || $cd == '192.234.90.30' || $cd == '207.98.173.137' || $cd == '41.105.89.201' || $cd == '76.16.247.148' || 
$cd == '72.224.38.131' || $cd == '107.207.168.139' || $cd == '74.178.203.10' || $cd == '115.188.31.8' ||
$cd == '41.86.47.110' || $cd == '196.212.137.106' || $cd == '173.148.241.100' || $cd == '216.161.34.66' || $cd == '78.30.140.46' ||
$cd == '70.89.165.90' || $cd == '105.244.2.239' || $cd == '99.40.158.38' || $cd == '166.127.1.183' || $cd == '75.140.73.31' ||
$cd == '108.253.114.241' || $cd == '184.148.125.248' || $cd == '67.142.168.20' || $cd == '75.73.24.53' || $cd == '75.150.128.145' ||
$cd == '31.44.74.15' || $cd == '68.61.138.12' || $cd == '184.58.217.202' || $cd == '216.108.167.105' || $cd == '65.128.88.4' ||
$cd == '173.88.202.65' || $cd == '155.150.223.145' || $cd == '90.148.79.225' || $cd == '65.222.120.131' || $cd == '108.203.58.63' ||
$cd == '24.36.38.226' || $cd == '67.100.110.142' || $cd == '24.146.181.173' || $cd == '67.66.140.11' || $cd == '87.197.108.65' ||
$cd == '173.171.212.157' || $cd == '64.45.232.245' || $cd == '70.44.74.102' || $cd == '186.151.70.188' || $cd == '108.52.10.7' ||
$cd == '94.7.150.196' || $cd == '98.246.180.156' || $cd == '195.162.84.230' || $cd == '82.23.97.142' || $cd == '76.188.165.25' ||
$cd == '50.33.170.48' || $cd == '87.247.93.104' || $cd == '66.153.178.38' || $cd == '108.88.142.65' || $cd == '98.196.193.117' || 
$cd == '69.112.29.146' || $cd == '74.5.31.212' || $cd == '159.83.168.254' || $cd == '70.94.43.227' || $cd == '167.135.14.181' || 
$cd == '204.87.40.1' || $cd == '66.193.126.2' || $cd == '64.79.177.254' || $cd == '68.70.45.234' || $cd == '212.174.168.205' || 
$cd == '107.3.168.202' || $cd == '153.26.178.60' || $cd == '68.104.160.17' || $cd == '71.197.125.186' || $cd == '70.127.197.98' || 
$cd == '96.241.135.17' || $cd == '108.41.200.194' || $cd == '83.113.3.210' || $cd == '67.242.203.223' || $cd == '120.32.87.79' || 
$cd == '76.26.242.51' || $cd == '71.28.97.48' || $cd == '68.19.5.183' || $cd == '2.80.243.38' || $cd == '210.242.215.239' || 
$cd == '210.242.215.197' || $cd == '210.242.215.234' || $cd == '74.78.17.54' || $cd == '69.74.209.200' || $cd == '70.224.44.247' || 
$cd == '173.12.24.182' || $cd == '173.189.240.210' || $cd == '69.137.87.90' || $cd == '98.77.0.140' || $cd == '72.131.70.61' || 
$cd == '188.24.195.157' || $cd == '27.253.10.188' || $cd == '2.99.53.36' || $cd == '163.205.78.43' || $cd == '216.100.215.5' || 
$cd == '98.157.82.169' || $cd == '76.177.132.66' || $cd == '112.200.158.249' || $cd == '72.89.55.253' || $cd == '100.2.147.164' || 
$cd == '66.226.193.122' || $cd == '184.38.175.149' || $cd == '67.171.28.23' || $cd == '24.151.232.199' || $cd == '173.76.181.125' || 
$cd == '204.87.40.1' || $cd == '212.174.168.205' || $cd == '174.1.13.59' || $cd == '168.170.196.120' || $cd == '167.135.14.181' || 
$cd == '64.79.177.254' || $cd == '68.70.45.234' || $cd == '204.63.207.110' || $cd == '79.112.78.68' || $cd == '173.88.202.65' ||
$cd == '155.150.223.145' || $cd == '209.62.220.42' || $cd == '75.169.7.19' || $cd == '92.253.85.35' || $cd == '198.254.16.200' || 
$cd == '209.34.114.199' || $cd == '41.205.94.185' || $cd == '81.215.231.121' || $cd == '110.171.101.86' || $cd == '46.185.219.237' || 
$cd == '128.83.25.251' || $cd == '24.92.152.95' || $cd == '152.79.255.1' || $cd == '216.103.134.250' || $cd == '31.11.121.48' || 
$cd == '67.159.36.18' || $cd == '67.159.36.19' || $cd == '67.159.36.20' || $cd == '67.159.36.21' || $cd == '67.159.36.22' || 
$cd == '67.159.36.23' || $cd == '67.159.36.24' || $cd == '67.159.36.25' || $cd == '67.159.36.26' || $cd == '67.159.36.27' || 
$cd == '67.159.36.28' || $cd == '67.159.36.29' || $cd == '74.63.112.138' || $cd == '74.63.112.139' || $cd == '74.63.112.140' ||
$cd == '74.63.112.141' || $cd == '74.63.112.142' || $cd == '74.63.112.143' || $cd == '74.63.112.144' || $cd == '674.63.112.145' ||
$cd == '74.63.112.146' || $cd == '74.63.112.147' || $cd == '95.154.230.252' || $cd == '95.154.230.253' || $cd == '95.154.230.254' ||
$cd == '67.159.36.30' || $cd == '74.3.160.86' || $cd == '67.230.166.0' || $cd == '67.230.166.1' || $cd == '67.230.166.2' || 
$cd == '67.230.166.3' || $cd == '67.230.166.100' || $cd == '67.230.166.4' || $cd == '67.230.166.5' || $cd == '67.230.166.6' || 
$cd == '67.230.166.7' || $cd == '67.230.166.8' || $cd == '67.230.166.10' || $cd == '67.230.166.11' || $cd == '67.230.166.12' || 
$cd == '67.230.166.13' || $cd == '67.230.166.14' || $cd == '67.230.166.15' || $cd == '67.230.166.16' || $cd == '67.230.166.17' || 
$cd == '67.230.166.20' || $cd == '67.230.166.21' || $cd == '67.230.166.64' || $cd == '67.230.166.27' || $cd == '67.230.166.28' || 
$cd == '67.230.166.22' || $cd == '67.230.166.23' || $cd == '67.230.166.24' || $cd == '67.230.166.25' || $cd == '67.230.166.26' || 
$cd == '67.230.166.29' || $cd == '67.230.166.30' || $cd == '67.230.166.31' || $cd == '67.230.166.32' || $cd == '67.230.166.33' || 
$cd == '67.230.166.34' || $cd == '67.230.166.35' || $cd == '67.230.166.36' || $cd == '67.230.166.37' || $cd == '67.230.166.38' || 
$cd == '67.230.166.39' || $cd == '67.230.166.40' || $cd == '67.230.166.41' || $cd == '67.230.166.42' || $cd == '67.230.166.43' || 
$cd == '67.230.166.44' || $cd == '67.230.166.45' || $cd == '67.230.166.46' || $cd == '67.230.166.47' || $cd == '67.230.166.48' || 
$cd == '67.230.166.49' || $cd == '67.230.166.50' || $cd == '67.230.166.51' || $cd == '67.230.166.52' || $cd == '67.230.166.53' || 
$cd == '67.230.166.54' || $cd == '67.230.166.55' || $cd == '67.230.166.56' || $cd == '67.230.166.57' || $cd == '67.230.166.58' || 
$cd == '67.230.166.59' || $cd == '67.230.166.60' || $cd == '67.230.166.61' || $cd == '67.230.166.62' || $cd == '67.230.166.63' ||  
$cd == '67.230.166.65' || $cd == '67.230.166.66' ||$cd == '67.230.166.67' || $cd == '67.230.166.68' || $cd == '67.230.166.69' || 
$cd == '67.230.166.70' || $cd == '67.230.166.71' || $cd == '67.230.166.72' || $cd == '67.230.166.73' || $cd == '67.230.166.74' || 
$cd == '67.230.166.75' || $cd == '67.230.166.76' || $cd == '67.230.166.77' || $cd == '67.230.166.78' || $cd == '67.230.166.79' || 
$cd == '67.230.166.80' || $cd == '67.230.166.81' || $cd == '67.230.166.82' || $cd == '67.230.166.83' || $cd == '67.230.166.84' || 
$cd == '67.230.166.85' || $cd == '67.230.166.86' || $cd == '67.230.166.87' || $cd == '67.230.166.88' || $cd == '67.230.166.89' || 
$cd == '67.230.166.90' || $cd == '67.230.166.91' || $cd == '67.230.166.92' || $cd == '67.230.166.93' || $cd == '67.230.166.94' || 
$cd == '67.230.166.95' || $cd == '67.230.166.96' || $cd == '67.230.166.97' || $cd == '67.230.166.98' || $cd == '67.230.166.99' || 
$cd == '67.230.166.9' || $cd == '67.230.166.18' || $cd == '67.230.166.19' || $cd == '65.49.68.175' || $cd == '180.246.215.206' || 
$cd == '117.195.1.64' || $cd == '117.195.1.64' || $cd == '67.159.56.162' || $cd == '67.159.56.163' || $cd == '67.159.56.164' || 
$cd == '67.159.56.165' || $cd == '67.159.56.166' || $cd == '74.63.86.218' || $cd == '74.63.86.219' || $cd == '74.63.86.220' || 
$cd == '74.63.86.221' || $cd == '74.63.86.222' || $cd == '74.63.112.148' || $cd == '74.63.112.149' || $cd == '74.63.112.150' || 
$cd == '74.63.112.151' || $cd == '74.63.112.152' || $cd == '74.63.112.153' || $cd == '74.63.112.154' || $cd == '74.63.112.155' || 
$cd == '74.63.112.156' || $cd == '76.205.48.181' || $cd == '68.67.252.210' || $cd == '24.2.202.197' || $cd == '173.166.198.114' || 
$cd == '70.139.7.6' || $cd == '68.197.29.129' || $cd == '98.222.28.54' || $cd == '74.215.64.83' || $cd == '24.11.249.22' || 
$cd == '205.161.125.254' || $cd == '67.131.53.93' || $cd == '75.145.10.94' || $cd == '24.8.128.105' || $cd == '99.67.67.16' || 
$cd == '50.151.106.96' || $cd == '99.165.45.83' || $cd == '78.149.59.87' || $cd == '24.214.163.44' || $cd == '74.129.115.230' || 
$cd == '81.135.103.193' || $cd == '2.223.205.122' || $cd == '68.52.215.55' || $cd == '108.134.141.138' || $cd == '190.137.193.158' || 
$cd == '81.141.246.221' || $cd == '50.128.198.188' || $cd == '71.234.148.240') {
if($uid == 1 || $ur == 'editor' || $ur == 'author') {
return false;
} else {
return true;
}
} else {
return false;
}
}
// End of iblocked ---------------------------------------------------	
?>