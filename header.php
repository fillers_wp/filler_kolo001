<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
    <head profile="http://gmpg.org/xfn/11">
	<title><?php // Search Title
if(is_search()) {
 $theqs = str_replace(' ', '+', str_replace('%20', '+', get_query_var('s'))); 
 js_seo($s, 'search','2',false);

//Category Page
} elseif(is_category()) {
  $thecat = single_cat_title('',false);
  js_seo($thecat, 'category','3',false);

//Tag Pages
} elseif(is_tag()) {
  $thetag = single_tag_title('',false);
  js_seo($thetag, 'tag','3',false);

//Archive Pages
} elseif(is_archive()) {
  //Daily Archive Pages
  if(is_day()) {
    js_seo('', 'isday','3',false);

  //Monthly Archive Pages
  } elseif(is_month()) {
    $thismonth = the_time('F, Y').' Archive';
    js_seo('', 'ismonth','3',false);

  //Yearly Archive Pages
  } elseif(is_year()) {
    js_seo('', 'isyear','3',false);
  }
// Back to Original WP Title
} else {wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name');}
?>
</title>
<?php /**
** Meta Description + Meta Keywords for Search, Category, Archive and Tag pages.
**/
//Search Page

if (is_search()) {
 $theqs = str_replace(' ', '+', str_replace('%20', '+', get_query_var('s'))); 
?>
<meta name="description" content="<?php js_seo($theqs, 'search','4',true); ?>" />
<meta name="keywords" content="<?php js_seo($theqs, 'search','4',true); ?>" />
<?php //Category Page
} elseif(is_category()) {
  $thecat = single_cat_title('',false);
?>
<meta name="description" content="<?php js_seo($thecat, 'category','5',true); ?>" />
<meta name="keywords" content="<?php js_seo($thecat, 'category','5',true); ?>" />
<?php //Category Page
} elseif(is_tag()) {
  $thetag = single_tag_title('',false);
?>
<meta name="description" content="<?php js_seo($thetag, 'tag','5',true); ?>" />
<meta name="keywords" content="<?php js_seo($thetag, 'tag','5',true); ?>" />
<?php //Archive Page
} elseif(is_archive()) {
  //Daily Archive
  if(is_day()) { ?>
<meta name="description" content="<?php js_seo('', 'isday','5',true); ?>" />
<meta name="keywords" content="<?php the_time('F jS, Y'); ?><?php js_seo('', 'isday','5',true); ?>" />
<?php //Monthly Archive
  } elseif(is_month()) { ?>
<meta name="description" content="<?php js_seo($thismonth, 'ismonth','5',true); ?>" />
<meta name="keywords" content="<?php the_time('F, Y'); ?><?php js_seo($thismonth, 'ismonth','5',true); ?>" />
<?php //Yearly Archive
  } elseif(is_year()) { ?>
<meta name="description" content="<?php js_seo('', 'isyear','5',true); ?>" />
<meta name="keywords" content="<?php js_seo('', 'isyear','5',true); ?>" /><?php 
  } else {}

//Attachment Page
} elseif(is_attachment()) { ?>
<meta name="description" content="<?php
$deattid = $post->ID; 
$dpost = get_post($deattid);
$dtitle = $dpost->post_title;
$dcontent = $dpost->post_content;
$dcontent = ucfirst(strtolower($dcontent));

//We have Parent's post ID
$prid = $dpost->post_parent;
$parr = get_post($prid);

$prtitle = $parr->post_title;
$posttags = get_the_tags($prid);
$dtags = '';
if ($posttags) {
  foreach($posttags as $tag) {
    $tagname = $tag->name;
    $dtags .= $tagname.','; 
  }
}
$dtags .= ']';
//We have the Tags
$detags = str_replace(',]','',$dtags);
//Construct Meta Desc.
$mtdesc = $prtitle.' : '.$dtitle.'. '.$dcontent.'. '.$detags;
echo $mtdesc;
?>" />

<?php
}
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<?php
//NOINDEX, NOFOLLOW : privacy, terms, copyright, 404
if(is_page('privacy-policy') || is_page('terms-conditions') || is_page('dmca-disclaimer') || is_page('copyright') || is_404()) {
?>
<meta name="robots" content="noindex, nofollow" />
<?php
//noindex, follow : about, contact, sitemap, search
} elseif(is_page('about-us') || is_page('contact-us') || is_page('sitemap') || is_search()) {
?>
<meta name="robots" content="noindex, follow" />
<?php
//index, follow
} else {
?>
<meta name="robots" content="index, follow" />
<?php
}
?>

        <meta http-equiv="content-type" content="<?php bloginfo('html_type') ?>; charset=<?php bloginfo('charset') ?>" />
	<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link href='<?php $td_favicon = get_option('td_favicon'); echo stripslashes($td_favicon); ?>' rel='shortcut icon'/>
        <link href='<?php $td_favicon = get_option('td_favicon'); echo stripslashes($td_favicon); ?>' rel='icon' type='image/ico'/>

	<?php if(is_singular()) { wp_enqueue_script('comment-reply');}?>

<?php wp_head(); ?>

<div align="center"><noscript>
   <div style="position:fixed; top:0px; left:0px; z-index:3000; height:100%; width:100%; background-color:#FFFFFF">
   <div style="font-family: Tahoma; font-size: 14px; background-color:#FFF000; padding: 10pt;">To see this page as it is meant to appear, we ask that you please enable your Javascript!</div></div>
</noscript></div>
<!--<script type="text/javascript">
function mousedwn(e) {
try { if (event.button == 2||event.button == 3) return false; }
catch (e) { if (e.which == 3) return false; }
}
    document.oncontextmenu = function() { return false; }
    document.ondragstart   = function() { return false; }
    document.onmousedown   = mousedwn;
</script>
<script type="text/javascript">
if (top.location != self.location) top.location.replace(self.location);
</script>
<script type="text/javascript">
document.ondragstart = function(){return false;};
</script>
<style type="text/css">
* : (input, textarea) {


    -webkit-touch-callout: none;
    -webkit-user-select: none;
}


img {
        -webkit-touch-callout: none;
        -webkit-user-select: none;        
    }
</style>


    <script type='text/javascript'>
window.addEventListener("keydown",function (e) {
    if (e.ctrlKey && (e.which == 65 || e.which == 67 || e.which == 85 || e.which == 80)) {
        e.preventDefault();
    }
})
        document.keypress = function(e) {
        if (e.ctrlKey && (e.which == 65 || e.which == 67 || e.which == 85 || e.which == 80)) {
    }
        return false;
                };
</script>-->
</head>
<body <?php body_class(); ?>>
<?php include('floatingads.php'); ?>
<div id="page">

<div id="header">
	
	
		<div id="header_left">
			<?php if (is_single() OR is_page()) { ?>
				<h2 id="logo"><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h2>
				
			<?php
			} else { ?>
				<h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a></h1>

			<?php }	?>
		</div>
	<?php if(get_option('business_ads-1')!=""){?>
		<div id="header_right">		
		<div class="ads-1">
				<?php if (get_option('business_ads-1') <> "") { 
				echo stripslashes(stripslashes(get_option('business_ads-1'))); 
				} ?>
		</div>
		</div>
		<?php } ?>
		<div class="clear"></div>
	
</div>

		<div id="nav_link"><!-- links -->
		
		<ul class="nav"><!-- Navigation Links -->
		    <?php if ( is_home() ) { ?>
  				<li class="first"><a href="<?php echo get_option('home'); ?>" rel='nofollow'>Home</a></li>
  			<?php } else { ?>
  				<li><a href="<?php echo get_option('home'); ?>"rel='nofollow'>Home</a></li>
  			<?php } ?>
			
		
<?php
// display 5 random categories
$cats ='';
$categories=get_categories();
$rand_keys = array_rand($categories, 5);
foreach ($rand_keys as $key) {
$cats .= $categories[$key]->term_id .',';
}
wp_list_categories('title_li=&hierarchical=0&hide_empty=0&include='.$cats);
?>
</ul><!-- End Navigation -->
		
	    <div id="search"><!-- Search Form -->
		<form method="get" id="searchform" action="<?php bloginfo('url'); ?>">
			<input type="text" class="field" name="s" id="searchh"  value="Search in this site..." onfocus="if (this.value == 'Search in this site...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search in this site...';}" />
			<input class="submit btn" type="image" src="<?php bloginfo('template_directory'); ?>/images/icon_search.png" value="Go" />
		</form>
	</div> <!--end search -->
		
	<div class="clear"></div>
		</div><!-- end links -->