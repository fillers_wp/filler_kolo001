<?php get_header(); ?>
<div id="wrapper">
	<div id="content">
		<?php if(get_option('business_ads-4')!=""){?>
<div class="ads-4">
	<?php if (get_option('business_ads-4') <> "") { 
		echo stripslashes(stripslashes(get_option('business_ads-4'))); 
} ?>
	</div>
<?php }?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
		<div class="post" id="post-<?php the_ID(); ?>">
 <div id="breadcrumbs">
<?php breadcrumbs(); ?>
</div>
			<h1 class="post-title"><?php the_title(); ?></h1>


<br />	
<br />		
<div class="posted-single">
<?php if ( $images = get_children(array(
		'post_parent' => get_the_ID(),
		'post_type' => 'attachment',
		'numberposts' => 1,
		'post_mime_type' => 'image',
		'orderby' => 'rand',
	))) : ?>
		<?php foreach( $images as $image ) :  ?>
			<?php echo wp_get_attachment_link($image->ID,$post->ID, 'large'); ?>
		<?php endforeach; ?>
	
<?php else: // No images ?>
	<!-- This post has no attached images -->
<?php endif; ?>	
<!--<ul class="lb-albumsgl">
<li>
        <a href="#image-1">
<span></span> 
        </a>
</li>
        <div class="lb-overlaysgl" id="image-1">
<?php //echo wp_get_attachment_link($image->ID,$post->ID, 'large'); ?>

            <div>
                <h3></h3>
            </div>
            <a rel="nofollow" href="#page" class="lb-closesgl">x Close</a>
        </div>

</ul>-->

</div>
<div class="entry">
<?php 
if($iblocked) {} else { ?>
<br />

<br />
<?php } ?>
<?php
    $content = apply_filters('the_content', $post->post_content);  //get the post content store in $content
    $save = explode("</p>", $content);  //Separate the content into <p> blocks
    $tcount=0; //this is count for number of <p> blocks
    $adon=0;  //this is a variable so you don't show ads more than once.
      foreach($save as $item) {
        echo $item;  //print the <p> block
        echo "</p>";
        if(preg_match('/<p> /',$item)==0 && $tcount>=1 && $adon==0) {
           $adon=1;
    ?>
        <div class="posted-single">
<?php if ( $images = get_children(array(
		'post_parent' => get_the_ID(),
		'post_type' => 'attachment',
		'numberposts' => 1,
		'post_mime_type' => 'image',
		'orderby' => 'rand',
	))) : ?>
		<?php foreach( $images as $image ) :  ?>
			<?php echo wp_get_attachment_link($image->ID,$post->ID, 'large'); ?>
		<?php endforeach; ?>
	
<?php else: // No images ?>
	<!-- This post has no attached images -->
<?php endif; ?>	

</div>

    <?php
        }
     if(preg_match('/<p> /',$item)==0 && $tcount>=4 && $adon==1) {
        $adon=2;
    ?>
         <div class="posted-single">
<?php if ( $images = get_children(array(
		'post_parent' => get_the_ID(),
		'post_type' => 'attachment',
		'numberposts' => 1,
		'post_mime_type' => 'image',
		'orderby' => 'rand',
	))) : ?>
		<?php foreach( $images as $image ) :  ?>
			<?php echo wp_get_attachment_link($image->ID,$post->ID, 'large'); ?>
		<?php endforeach; ?>
	
<?php else: // No images ?>
	<!-- This post has no attached images -->
<?php endif; ?>	

</div>
    <?php
       }
       $tcount++;
     }
?> 
			
				
			</div>
			
			<div class="clear"></div>
		<div id="gallerysingle">
<div id="gallerysingl"><h2><?php 
global $post;
$attachments = get_children( array( 'post_parent' => get_the_ID(), 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) );

$count = count( $attachments );
$specific = array();
$i = 1;

foreach ( $attachments as $attachment ) {
    $specific[$attachment->ID] = $i;
    ++$i;
}

echo "{$specific[$post->ID]} {$count}";

?> Photos of the <?php the_title(); ?></h2></div>
<div class="gambar1">
<?php gallerysingle();?>

</div>
</div>
<div class="clear"></div>
<div class="social-btn">


			
		</div>
				<p class="postmetadata"><?php the_time('F j, Y') ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php the_category(', ') ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php comments_popup_link('No Comments', '1 Comment ', '% Comments'); ?> &nbsp;&nbsp;&asymp;&nbsp;&nbsp; <?php sgl_tags();?></p>
			
		</div>
		
		
<div class="related">
<?php
$backup = $post;
$tags = wp_get_post_tags($post->ID);
if ($tags) {
$tag_ids = array();
foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
$args=array(
'tag__in' => $tag_ids,
'post__not_in' => array($post->ID),
'showposts'=>4, // Jumlah related posts yang tampil (Rekomendasi: 4).
'orderby'=>rand,
'caller_get_posts'=>1
);
$my_query = new wp_query($args);
if( $my_query->have_posts() ) {
echo '<h3>Related Post from '.get_the_title().'</h3><ul>';
while ($my_query->have_posts()) {
$my_query->the_post();
?>

<div class="rltdleft">
 <?php relateds();?>
</div>

<li><a href="<?php the_permalink() ?>" ><?php if ( get_the_title() ){ the_title(); } else { echo "Tidak ada artikel terkait"; } ?></a> </li> 

<div class="related-p">

 <?php an_excerpt('an_excerptlength_related', ''); ?>
</div>


<div id="rpcatp">
<?php
foreach((get_the_category()) as $category) {
echo $category->cat_name . ' Category ';
}
?>

</div>
<div style="clear: both"></div>
<?php
}
echo '</ul>';
}
}
$post = $backup;
wp_reset_query();
?>
</div>

<div style="clear: both"></div>

	<?php comments_template(); ?>
	
	<?php endwhile; else: ?>
	
		<h2 class="center">Not Found</h2>
	
<?php endif; ?>
	
<div class="navigation">

						<div class="alignleft"><?php previous_post_link('&laquo; %link') ?></div>
			<div class="alignright"><?php next_post_link('%link &raquo;') ?></div>
			
		</div>
	</div>
	
<?php get_sidebar(); ?>
</div>

<?php get_footer(); ?>